package fr.nfleuret.yukabis

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import fr.nfleuret.yukabis.models.Product

class ProductListAdapter (
    private val products : List<Product>,
    private val listener: OnListItemClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductListItem {
        return ProductListItem(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.product_list_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return products.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ProductListItem).bindValues(products[position])

        //set listener
        holder.itemView.setOnClickListener {
            listener.onItemClicked(position)
            //works as well:
            //listener.onItemClicked(holder.adapterPosition)
        }
    }
}