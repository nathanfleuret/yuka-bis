package fr.nfleuret.yukabis

import fr.nfleuret.yukabis.models.Product
import java.text.FieldPosition

interface OnListItemClickListener {
    fun onItemClicked(position: Int)
}