package fr.nfleuret.yukabis

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import fr.nfleuret.yukabis.fragment.ProductDetailsFragment
import fr.nfleuret.yukabis.fragment.ProductDetailsNutritionFragment
import fr.nfleuret.yukabis.fragment.ProductDetailsNutritionalValuesFragment
import fr.nfleuret.yukabis.fragment.ProductDetailsSummaryFragment
import fr.nfleuret.yukabis.models.Nutriscore
import fr.nfleuret.yukabis.models.NutritionFacts
import fr.nfleuret.yukabis.models.NutritionFactsItem
import fr.nfleuret.yukabis.models.Product

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        *TESTING IMPORT OF A FRAGMENT*
        supportFragmentManager
//            .beginTransaction()
//            .replace(R.id.container, ProductDetailsFragment())
//            .commit()

        //Gradiant color on Toolbar
        supportActionBar?.setBackgroundDrawable(
            ContextCompat.getDrawable(
                this,
                R.drawable.toolbar_gradient
            )
        )

//        Toast.makeText(this, "Hello World", Toast.LENGTH_SHORT).show()
//        Log.v("START", "Hello World")

    }

}

