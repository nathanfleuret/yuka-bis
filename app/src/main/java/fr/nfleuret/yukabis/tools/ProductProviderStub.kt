package fr.nfleuret.yukabis.tools

import fr.nfleuret.yukabis.models.Nutriscore
import fr.nfleuret.yukabis.models.NutritionFacts
import fr.nfleuret.yukabis.models.NutritionFactsItem
import fr.nfleuret.yukabis.models.Product

class ProductProviderStub {

    companion object {
        fun getProduct(): Product {
            val nutritionFacts = NutritionFacts(
                energy = NutritionFactsItem("kj", 293.0),
                lipid = NutritionFactsItem("g", 0.8),
                saturatedFattyAcid = NutritionFactsItem("g", 0.1),
                glucid = NutritionFactsItem("g", 8.4),
                sugar = NutritionFactsItem("g", 5.2),
                dietaryFiber = NutritionFactsItem("g", 5.2),
                protein = NutritionFactsItem("g", 4.8),
                salt = NutritionFactsItem("g", 0.75),
                sodium = NutritionFactsItem("g", 0.295),
            )
            return Product(
                name = "Petits pois et carottes",
                brand = "Cassegrain",
                barcode = "3083680085304",
                nutriscore = Nutriscore.A,
                //image = "https://fujifilm-x.com/wp-content/uploads/2019/08/x-t30_sample-images01.jpg",
                image = "https://upserve.com/media/sites/2/bigstock-Staff-showing-a-sample-of-chee-189121246.jpg",
                //KEEPS GETTING HTTP 504 with this image:
                //image = "https://static.openfoodfacts.org/images/products/308/368/008/5304/front_fr.7.400.jpg",
                quantity = "400 g (280 g net égoutté)",
                countries = listOf("France", "Japon", "Suisse"),
                ingredients = listOf(
                    "Petits pois 66%",
                    "eau",
                    "garniture 2,8% (salade, oignon grelot)",
                    "sucre",
                    "sel",
                    "arôme naturel"
                ),
                allergens = listOf("Aucune"),
                additives = listOf("Aucun"),
                nutritionFacts = nutritionFacts
            )
        }

        fun getProductList(): List<Product> {
            val product1 = getProduct()
            return listOf(product1, product1, product1)
            //return emptyList()
        }
    }
}