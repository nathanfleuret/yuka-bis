package fr.nfleuret.yukabis.tools

import android.graphics.Typeface
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan

class StringBuilder {
    companion object {
        fun setTextBold(text: String, sep: String = " : "): SpannableStringBuilder {
            val str = SpannableStringBuilder(text)
            str.setSpan(StyleSpan(Typeface.BOLD), 0, text.indexOf(sep) + sep.length, 0)
            return str;
        }
    }
}