package fr.nfleuret.yukabis.fragment

import android.content.res.ColorStateList
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.graphics.drawable.DrawableCompat
import androidx.fragment.app.Fragment
import fr.nfleuret.yukabis.R
import fr.nfleuret.yukabis.models.*
import fr.nfleuret.yukabis.tools.ProductProviderStub
import kotlinx.android.synthetic.main.fragment_product_details_nutrition.*

class ProductDetailsNutritionFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_product_details_nutrition, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val product: Product = ProductDetailsFragmentArgs.fromBundle(
            requireParentFragment().requireParentFragment().requireArguments()
        ).product
        bindValues(product)
    }

    private fun bindValues(product: Product) {
        //assign product props to view
        val lipid = NutrientItem(product.nutritionFacts.lipid, NutrientCategory.Lipid)
        val saturatedFattyAcid = NutrientItem(product.nutritionFacts.saturatedFattyAcid, NutrientCategory.SaturatedFattyAcid)
        val sugar = NutrientItem(product.nutritionFacts.sugar, NutrientCategory.Sugar)
        val salt = NutrientItem(product.nutritionFacts.salt, NutrientCategory.Salt)

        ItemNutrition_lipid_qty.text = getString(R.string.lipid_qty, product.nutritionFacts.lipid.amountPer100gToString())
        ItemNutrition_acid_qty.text = getString(R.string.acid_qty,
            product.nutritionFacts.saturatedFattyAcid.amountPer100gToString())
        ItemNutrition_sugar_qty.text = getString(R.string.sugar_qty,
            product.nutritionFacts.sugar.amountPer100gToString())
        ItemNutrition_salt_qty.text = getString(R.string.salt_qty,
            product.nutritionFacts.salt.amountPer100gToString())

        lipid_text_level.text = getString(lipid.stringRes)
        acid_text_level.text = getString(saturatedFattyAcid.stringRes)
        sugar_text_level.text = getString(sugar.stringRes)
        salt_text_level.text = getString(salt.stringRes)

        //set color to oval_shape:
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            DrawableCompat.setTintList(lipid_level.background, ColorStateList.valueOf(resources.getColor(lipid.color, null)))
            DrawableCompat.setTintList(saturatedFattyAcid_level.background, ColorStateList.valueOf(resources.getColor(saturatedFattyAcid.color, null)))
            DrawableCompat.setTintList(sugar_level.background, ColorStateList.valueOf(resources.getColor(sugar.color, null)))
            DrawableCompat.setTintList(salt_level.background, ColorStateList.valueOf(resources.getColor(salt.color, null)))
        } else {
            DrawableCompat.setTintList(lipid_level.background, ColorStateList.valueOf(resources.getColor(lipid.color)))
            DrawableCompat.setTintList(saturatedFattyAcid_level.background, ColorStateList.valueOf(resources.getColor(saturatedFattyAcid.color)))
            DrawableCompat.setTintList(sugar_level.background, ColorStateList.valueOf(resources.getColor(sugar.color)))
            DrawableCompat.setTintList(salt_level.background, ColorStateList.valueOf(resources.getColor(salt.color)))
        }
    }
}