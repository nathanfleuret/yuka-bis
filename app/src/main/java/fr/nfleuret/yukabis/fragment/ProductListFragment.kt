package fr.nfleuret.yukabis.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import fr.nfleuret.yukabis.OnListItemClickListener
import fr.nfleuret.yukabis.ProductListAdapter
import fr.nfleuret.yukabis.R
import fr.nfleuret.yukabis.models.Product
import fr.nfleuret.yukabis.tools.ProductProviderStub
import kotlinx.android.synthetic.main.fragment_empty_product_list.*
import kotlinx.android.synthetic.main.fragment_product_list.*
import kotlinx.android.synthetic.main.fragment_product_list.products_start_scan

class ProductListFragment : Fragment() {
    val products: List<Product> = ProductProviderStub.getProductList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return if (products.isEmpty()) {
            //Log.v("TEST", "list empty")
            inflater.inflate(
                R.layout.fragment_empty_product_list,
                container,
                false
            )
        } else {
            inflater.inflate(
                R.layout.fragment_product_list,
                container,
                false
            )
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (products.isNotEmpty()) {
            product_list.run {
                layoutManager = LinearLayoutManager(context)

                adapter = ProductListAdapter(products, object : OnListItemClickListener {
                    override fun onItemClicked(position: Int) {
                        Log.v("CLIC", position.toString())
                        findNavController().navigate(
                            ProductListFragmentDirections.actionProductListFragmentToProductDetailsFragment(
                                products[position]
                            )
                        )
                    }
                })
            }
            //INTENT
            products_start_scan.setOnClickListener {
                OnClickScan()
            }
        }
        else {
            //empty
            products_start_scan.background = ContextCompat.getDrawable(requireContext(), R.drawable.button_gradient)
            //INTENT
            products_start_scan.setOnClickListener {
                OnClickScan()
            }
        }
    }

    fun OnClickScan() {
        // Clic sur le bouton Scan
        Log.v("CLIC", "Clicked scan button")
        val intent = Intent()
        intent.action = "com.google.zxing.client.android.SCAN"
        intent.putExtra("SCAN_FORMATS", "EAN_13")
        startActivityForResult(intent, 100)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 100 && data != null) {
            val scanResultFormat = data.getStringExtra("SCAN_RESULT_FORMAT")
            val scanResult = data.getStringExtra("SCAN_RESULT")

            //TODO Call API to fetch product data from ScanResult
            val productFromAPI = ProductProviderStub.getProduct()

            findNavController().navigate(
                ProductListFragmentDirections.actionProductListFragmentToProductDetailsFragment(
                    productFromAPI
                )
            )
        }
    }
}