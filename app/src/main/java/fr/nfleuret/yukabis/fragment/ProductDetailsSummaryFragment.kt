package fr.nfleuret.yukabis.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.squareup.picasso.Picasso
import fr.nfleuret.yukabis.R
import fr.nfleuret.yukabis.models.Nutriscore
import fr.nfleuret.yukabis.models.NutritionFacts
import fr.nfleuret.yukabis.models.NutritionFactsItem
import fr.nfleuret.yukabis.models.Product
import fr.nfleuret.yukabis.tools.ProductProviderStub
import fr.nfleuret.yukabis.tools.StringBuilder
import kotlinx.android.synthetic.main.fragment_product_details_summary.*

class ProductDetailsSummaryFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            R.layout.fragment_product_details_summary,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val product: Product = ProductDetailsFragmentArgs.fromBundle(
            requireParentFragment().requireParentFragment().requireArguments()
        ).product
        bindValues(product)
    }

    private fun bindValues(product: Product) {
        //assign product props to view
        Picasso.get().load(product.image).into(ProductImage)
        TitleView.text = getString(R.string.food_name, product.name)
        BrandView.text = getString(R.string.food_brand, product.brand)
        BarcodeView.text = StringBuilder.setTextBold(getString(R.string.barcode, product.barcode))
        QuantityView.text =
            StringBuilder.setTextBold(getString(R.string.quantity, product.quantity))
        LocationView.text = StringBuilder.setTextBold(
            getString(
                R.string.selling_locations,
                product.countries.joinToString()
            )
        )
        IngredientsView.text = StringBuilder.setTextBold(
            getString(
                R.string.selling_locations,
                product.ingredients.joinToString()
            )
        )
        AllergensView.text = StringBuilder.setTextBold(
            getString(
                R.string.allergens,
                product.allergens.joinToString()
            )
        )
        AdditivesView.text = StringBuilder.setTextBold(
            getString(
                R.string.additives,
                product.additives.joinToString()
            )
        )
        setNutriscoreImage(product.nutriscore)
    }

    private fun setNutriscoreImage(nutriscore: Nutriscore) {
        when (nutriscore) {
            Nutriscore.A -> NutriscoreView.setImageResource(R.drawable.nutriscore_a)
            Nutriscore.B -> NutriscoreView.setImageResource(R.drawable.nutriscore_b)
            Nutriscore.C -> NutriscoreView.setImageResource(R.drawable.nutriscore_c)
            Nutriscore.D -> NutriscoreView.setImageResource(R.drawable.nutriscore_d)
            Nutriscore.E -> NutriscoreView.setImageResource(R.drawable.nutriscore_e)
        }
    }
}