package fr.nfleuret.yukabis.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import fr.nfleuret.yukabis.R
import fr.nfleuret.yukabis.models.Nutriscore
import fr.nfleuret.yukabis.models.NutritionFacts
import fr.nfleuret.yukabis.models.NutritionFactsItem
import fr.nfleuret.yukabis.models.Product
import fr.nfleuret.yukabis.tools.ProductProviderStub
import kotlinx.android.synthetic.main.fragment_product_details_nutritional_values.*

class ProductDetailsNutritionalValuesFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            R.layout.fragment_product_details_nutritional_values,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val product: Product = ProductDetailsFragmentArgs.fromBundle(
            requireParentFragment().requireParentFragment().requireArguments()
        ).product
        bindValues(product)
    }

    private fun bindValues(product: Product) {
        //assign product props to view
        energy100.text = product.nutritionFacts.energy.amountPer100gToString()
        energyPortion.text = product.nutritionFacts.energy.amountPerPortionToString()
        fat100.text = product.nutritionFacts.lipid.amountPer100gToString()
        fatPortion.text = product.nutritionFacts.lipid.amountPerPortionToString()
        saturatedFat100.text = product.nutritionFacts.saturatedFattyAcid.amountPer100gToString()
        saturatedFatPortion.text = product.nutritionFacts.saturatedFattyAcid.amountPerPortionToString()
        glucid100.text = product.nutritionFacts.glucid.amountPer100gToString()
        glucidPortion.text = product.nutritionFacts.glucid.amountPerPortionToString()
        sugar100.text = product.nutritionFacts.sugar.amountPer100gToString()
        sugarPortion.text = product.nutritionFacts.sugar.amountPerPortionToString()
        fiber100.text = product.nutritionFacts.dietaryFiber.amountPer100gToString()
        fiberPortion.text = product.nutritionFacts.dietaryFiber.amountPerPortionToString()
        protein100.text = product.nutritionFacts.protein.amountPer100gToString()
        proteinPortion.text = product.nutritionFacts.protein.amountPerPortionToString()
        salt100.text = product.nutritionFacts.salt.amountPer100gToString()
        saltPortion.text = product.nutritionFacts.salt.amountPerPortionToString()
        sodium100.text = product.nutritionFacts.sodium.amountPer100gToString()
        sodiumPortion.text = product.nutritionFacts.sodium.amountPerPortionToString()
    }
}