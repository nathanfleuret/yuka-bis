package fr.nfleuret.yukabis.models

import androidx.annotation.StringRes
import fr.nfleuret.yukabis.R

class NutrientItem(nutritionItem: NutritionFactsItem, category: NutrientCategory){
    val level: NutrientLevel
    val color: Int
    val stringRes: Int

    init{
        level = setNutrientLevel(nutritionItem, category)
        color = setColor()
        stringRes = setStringRes()
    }

    private fun setStringRes(): Int {
        return when (level) {
            NutrientLevel.Low -> R.string.nutrition_low
            NutrientLevel.Medium -> R.string.nutrition_medium
            NutrientLevel.High -> R.string.nutrition_high
        }
    }

    private fun setNutrientLevel(nutritionItem: NutritionFactsItem, category: NutrientCategory): NutrientLevel {
        when (category) {
            NutrientCategory.Lipid ->
                return when {
                    nutritionItem.amountPer100g < 3.0 -> NutrientLevel.Low
                    nutritionItem.amountPer100g in 3.0..20.0 -> NutrientLevel.Medium
                    nutritionItem.amountPer100g > 20.0 -> NutrientLevel.High
                    else -> { throw Exception() }
                }
            NutrientCategory.SaturatedFattyAcid ->
                return when {
                    nutritionItem.amountPer100g < 1.5 -> NutrientLevel.Low
                    nutritionItem.amountPer100g in 1.5..5.0 -> NutrientLevel.Medium
                    nutritionItem.amountPer100g > 5.0 -> NutrientLevel.High
                    else -> { throw Exception() }
                }
            NutrientCategory.Sugar ->
                return when {
                    nutritionItem.amountPer100g < 5.0 -> NutrientLevel.Low
                    nutritionItem.amountPer100g in 5.0..12.5 -> NutrientLevel.Medium
                    nutritionItem.amountPer100g > 12.5 -> NutrientLevel.High
                    else -> { throw Exception() }
                }
            NutrientCategory.Salt ->
                return when {
                    nutritionItem.amountPer100g < 0.3 -> NutrientLevel.Low
                    nutritionItem.amountPer100g in 0.3..1.5 -> NutrientLevel.Medium
                    nutritionItem.amountPer100g > 1.5 -> NutrientLevel.High
                    else -> { throw Exception() }
                }
        }
    }

    private fun setColor(): Int {
        return when (level) {
            NutrientLevel.Low -> R.color.nutrient_level_low
            NutrientLevel.Medium -> R.color.nutrient_level_moderate
            NutrientLevel.High -> R.color.nutrient_level_high
        }
    }
}

