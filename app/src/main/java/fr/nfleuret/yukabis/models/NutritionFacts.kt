package fr.nfleuret.yukabis.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NutritionFacts(
    var energy: NutritionFactsItem,
    var lipid: NutritionFactsItem,
    var saturatedFattyAcid: NutritionFactsItem,
    var glucid: NutritionFactsItem,
    var sugar: NutritionFactsItem,
    var dietaryFiber: NutritionFactsItem,
    var protein: NutritionFactsItem,
    var salt: NutritionFactsItem,
    var sodium: NutritionFactsItem,
) : Parcelable
