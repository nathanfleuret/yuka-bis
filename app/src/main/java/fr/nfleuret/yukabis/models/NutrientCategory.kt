package fr.nfleuret.yukabis.models

enum class NutrientCategory {
    Lipid,
    SaturatedFattyAcid,
    Sugar,
    Salt
}