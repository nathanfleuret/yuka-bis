package fr.nfleuret.yukabis.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NutritionFactsItem(
    var unit: String,
    var amountPer100g: Double,
    var amountPerPortion: Double? = null,
) : Parcelable {

    fun amountPer100gToString(): String = "$amountPer100g $unit"

    fun amountPerPortionToString(): String = if (amountPerPortion != null) "$amountPerPortion $unit" else "?"
}
