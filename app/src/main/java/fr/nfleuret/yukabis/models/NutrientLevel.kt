package fr.nfleuret.yukabis.models

enum class NutrientLevel {
    Low,
    Medium,
    High
}