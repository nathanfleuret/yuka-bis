package fr.nfleuret.yukabis.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Product(
    var name: String,
    var brand: String,
    var barcode: String,
    var nutriscore: Nutriscore,
    var image: String,
    var quantity: String,
    var countries: List<String>,
    var ingredients: List<String>,
    var allergens: List<String>,
    var additives: List<String>,
    var nutritionFacts: NutritionFacts
) : Parcelable

enum class Nutriscore() {
    A,
    B,
    C,
    D,
    E
}