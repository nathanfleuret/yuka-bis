package fr.nfleuret.yukabis

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import fr.nfleuret.yukabis.models.Product
import kotlinx.android.synthetic.main.product_list_item.view.*

class ProductListItem(v: View) : RecyclerView.ViewHolder(v) {

    fun bindValues(product: Product) {
        //assign product props to view
        Picasso.get().load(product.image).into(itemView.product_image)
        itemView.product_name.text = product.name
        itemView.product_brand.text = product.brand
        itemView.product_nutriscore.text =
            itemView.context.getString(R.string.nutriscore, product.nutriscore.toString())
        itemView.product_calories.text = itemView.context.getString(
            R.string.calories_per_portion,
            product.nutritionFacts.energy.amountPer100g.toString()
        )
    }
}